//2
db.fruits.aggregate([
	{$match:{ "onSale": true }},
        {$count : "fruitsonsale"}

])
//3
db.fruits.aggregate([
	{$match:{ "stock": {$gt:20 }}},
        {$count : "stock>20"}

])
//4
db.fruits.aggregate([
	{$match:{ "onSale": true}},
        {$group : {_id: null, avgprice: {$avg:"$price"}}}

])
//5
db.fruits.aggregate([
	{$match:{ "onSale": true}},
        {$group : {_id: null, maxprice: {$max:"$price"}}}

])
//6
db.fruits.aggregate([
	{$match:{ "onSale": true}},
        {$group : {_id: null, minprice: {$min:"$price"}}}

])